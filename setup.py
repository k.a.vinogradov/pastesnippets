from setuptools import setup

setup(
    name='pastesnippets',
    version='2.1',
    packages=['PyQt5', ],
    url='',
    license='CC',
    author='Constantine Vinogradov',
    author_email='k.a.vinogradov@gmail.com',
    description='small tray tool to help handling text templates'
)
