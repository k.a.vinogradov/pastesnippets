#!/usr/bin/env python

from paste_snippets.main import PopupWindow
from PyQt5.QtWidgets import QApplication
import sys

if __name__ == '__main__':
    app = QApplication(sys.argv)
    pw = PopupWindow()
    pw.hide()
    sys.exit(app.exec())
