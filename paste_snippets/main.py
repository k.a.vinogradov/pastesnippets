from paste_snippets.popup import Ui_Popup
from paste_snippets.snippets_editor import *
from paste_snippets.file_handler import JsonSnippet
from PyQt5.QtGui import QIcon, QGuiApplication
from PyQt5.QtWidgets import (QAction, QApplication, QMenu, QSystemTrayIcon,
                             QListWidgetItem, QMainWindow)
from paste_snippets.__init__ import ICONS_DIR


class PopupWindow(QMainWindow):

    def __init__(self, parent=None):

        super().__init__(parent=parent)

        self.ui = Ui_Popup()
        self.ui.setupUi(self)
        self.snippetsList = self.ui.listWidget

        self.snippetsList.itemDoubleClicked.connect(self.getCopyText)

        self.createActions()
        self.createTrayIcon()
        self.trayIcon.setIcon(QIcon(os.path.join(ICONS_DIR, 'snippets.png')))

        self.trayIcon.show()
        self.trayIcon.activated.connect(self.iconActivated)
        self.showSettings.triggered.connect(self.settings)
        self.quitAction.triggered.connect(QApplication.instance().quit)

    def createActions(self):
        self.showSettings = QAction("&Edit snippets", self)
        self.quitAction = QAction("&Close", self)

    def createTrayIcon(self):
        self.trayIconMenu = QMenu(self)
        self.trayIconMenu.addAction(self.showSettings)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.quitAction)

        self.trayIcon = QSystemTrayIcon(self)
        self.trayIcon.setContextMenu(self.trayIconMenu)

    def iconActivated(self, reason):
        if reason in (QSystemTrayIcon.Trigger, QSystemTrayIcon.DoubleClick):
            self.show()
        elif reason == QSystemTrayIcon.MiddleClick:
            pass

    def populateList(self):
        self.snippetsList.clear()
        snippets = JsonSnippet()
        snippets_list = snippets.snippets_list
        for attribs in snippets_list:
            item = QListWidgetItem()
            item.setIcon(QIcon(os.path.join(ICONS_DIR, attribs['icon'])))
            item.setText(attribs['name'])
            self.snippetsList.addItem(item)

    def getCopyText(self):
        index = self.snippetsList.currentRow()
        jsp = JsonSnippet()
        row = jsp.get_snippet(index)
        clipboard = QGuiApplication.clipboard()
        clipboard.setText(row['text'])
        self.hide()

    def settings(self):
        self.hide()
        s = ShowSnippetsEditor()
        s.exec()

    def closeEvent(self, event):
        self.hide()
        event.ignore()

    def showEvent(self, QShowEvent):
        self.populateList()
