# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './SnippetsEditor.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
from paste_snippets.file_handler import JsonSnippet
from PyQt5 import QtCore, QtGui, QtWidgets
import os
from paste_snippets.__init__ import RES_DIR, ICONS_DIR


class Ui_SnippetEditor(object):
    def setupUi(self, SnippetEditor):
        SnippetEditor.setGeometry(800, 400, 580, 530)
        SnippetEditor.setWindowTitle('Edit snippets')

        self.snippetsList = QtWidgets.QListWidget()
        self.snippetsList.setMinimumSize(QtCore.QSize(270, 471))
        self.snippetsList.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.snippetsList.setAlternatingRowColors(True)

        self.addSnippet = QtWidgets.QPushButton()
        self.addSnippet.setFixedSize(81, 35)
        self.addSnippet.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(os.path.join(ICONS_DIR, 'list-add.svg')), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addSnippet.setIcon(icon)
        self.removeSnippet = QtWidgets.QPushButton()
        self.removeSnippet.setFixedSize(51, 35)
        self.removeSnippet.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(os.path.join(ICONS_DIR, 'list-remove.svg')), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeSnippet.setIcon(icon1)
        self.closeButton = QtWidgets.QPushButton()
        self.closeButton.setFixedSize(90, 35)
        self.closeButton.setText('Close')

        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setFixedSize(61, 31)
        self.snippetName = QtWidgets.QLabel()
        self.snippetName.setText('Name')
        self.nameEdit = QtWidgets.QLineEdit()
        self.snippetBody = QtWidgets.QLabel()
        self.snippetBody.setText('Text for clipboard')
        self.textEdit = QtWidgets.QTextEdit()

        SnippetEditor.verticalLayoutRight = QtWidgets.QVBoxLayout()
        SnippetEditor.verticalLayoutRight.addWidget(self.comboBox)
        SnippetEditor.verticalLayoutRight.addWidget(self.snippetName)
        SnippetEditor.verticalLayoutRight.addWidget(self.nameEdit)
        SnippetEditor.verticalLayoutRight.addWidget(self.snippetBody)
        SnippetEditor.verticalLayoutRight.addWidget(self.textEdit)

        SnippetEditor.grid = QtWidgets.QGridLayout()
        SnippetEditor.grid.addWidget(self.snippetsList, 0, 0)

        SnippetEditor.horizontalLayoutBottom = QtWidgets.QHBoxLayout()
        SnippetEditor.horizontalLayoutBottom.addWidget(self.addSnippet)
        SnippetEditor.horizontalLayoutBottom.addWidget(self.removeSnippet)
        SnippetEditor.horizontalLayoutBottom.addStretch(1)
        SnippetEditor.horizontalLayoutBottom.addWidget(self.closeButton)

        SnippetEditor.grid.addLayout(SnippetEditor.verticalLayoutRight, 0, 1)
        SnippetEditor.grid.addLayout(SnippetEditor.horizontalLayoutBottom, 1, 0, 1, 2)

        SnippetEditor.setLayout(SnippetEditor.grid)


class ShowSnippetsEditor(QtWidgets.QDialog, Ui_SnippetEditor):

    def __init__(self):
        self.icons = os.listdir(ICONS_DIR)
        QtWidgets.QDialog.__init__(self)
        self.ui = Ui_SnippetEditor()
        self.ui.setupUi(self)
        self.snippetsList = self.ui.snippetsList
        self.populateComboBox()
        self.snippetsList.currentItemChanged.connect(self.getListClicked)

        self.ui.addSnippet.clicked.connect(self.addSnippet)
        self.ui.removeSnippet.clicked.connect(self.removeSnippet)

        self.ui.nameEdit.textEdited.connect(self.saveSnippet)
        self.ui.textEdit.textChanged.connect(self.saveSnippetText)
        self.ui.comboBox.activated.connect(self.saveSnippet)

        self.ui.closeButton.clicked.connect(self.close)

        self.jsp = JsonSnippet()

    def closeEvent(self, event):
        self.hide()
        event.ignore()

    def hideEvent(self, event):
        self.jsp.save_snippet_file()

    def showEvent(self, event):
        self.populateList()
        self.snippetsList.setCurrentRow(0)

    def populateComboBox(self):
        for icon in self.icons:
            icon = os.path.join(ICONS_DIR, icon)
            self.ui.comboBox.addItem(QtGui.QIcon(icon), '')

    def populateList(self):
        self.snippetsList.clear()
        snippets = self.jsp.snippets_list
        for attribs in snippets:
            item = QtWidgets.QListWidgetItem()
            item.setIcon(QtGui.QIcon(os.path.join(ICONS_DIR, attribs['icon'])))
            item.setText(attribs['name'])
            self.snippetsList.addItem(item)

    def getListClicked(self):
        index = self.snippetsList.currentRow()
        row = self.jsp.get_snippet(index)
        self.ui.nameEdit.setText(row['name'])
        self.ui.textEdit.blockSignals(True)
        self.ui.textEdit.setText(row['text'])
        self.ui.textEdit.blockSignals(False)
        cb_curr_index = self.icons.index(row['icon'])
        self.ui.comboBox.setCurrentIndex(cb_curr_index)

    def addSnippet(self):
        self.jsp.add_snippet()
        self.populateList()
        index = len(self.jsp.snippets_list) - 1
        self.snippetsList.setCurrentRow(index)

    def removeSnippet(self):
        index = self.snippetsList.currentRow()
        self.jsp.remove_snippet(index)
        self.populateList()
        if index != 0:
            self.snippetsList.setCurrentRow(index - 1)
        else:
            self.snippetsList.setCurrentRow(0)

    def saveSnippet(self):
        index = self.snippetsList.currentRow()
        snippet_name = str(self.ui.nameEdit.text())
        snippet_icon = self.icons[self.ui.comboBox.currentIndex()]
        snippet_text = self.ui.textEdit.toPlainText()
        self.jsp.save_snippet(index=index, name=snippet_name, icon=snippet_icon, text=snippet_text)
        self.populateList()
        self.snippetsList.setCurrentRow(index)

    def saveSnippetText(self):
        index = self.snippetsList.currentRow()
        snippet_text = self.ui.textEdit.toPlainText()
        self.jsp.save_snippet_text(index=index, text=snippet_text)
