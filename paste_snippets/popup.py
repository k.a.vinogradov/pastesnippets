# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './popup.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Popup(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        self.listWidget = None
        self.centralWidget = None

    def setupUi(self, Popup):
        Popup.setGeometry(1500, 700, 263, 294)
        Popup.setWindowTitle('Snippets')

        self.centralWidget = QtWidgets.QWidget(Popup)
        self.centralWidget.setObjectName("centralWidget")
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setObjectName("snippetsList")
        self.listWidget.setMinimumSize(QtCore.QSize(261, 291))
        self.listWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.listWidget.setAlternatingRowColors(True)
        self.listWidget.setModelColumn(0)

        self.vbox = QtWidgets.QHBoxLayout()
        self.vbox.addWidget(self.listWidget)

        self.centralWidget.setLayout(self.vbox)

        Popup.setCentralWidget(self.centralWidget)
