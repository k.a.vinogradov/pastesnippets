import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
RES_DIR = os.path.join(BASE_DIR, 'resources')
ICONS_DIR = os.path.join(RES_DIR, 'icons')
