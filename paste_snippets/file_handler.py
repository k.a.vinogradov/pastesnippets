import os
import json
from paste_snippets.__init__ import RES_DIR, ICONS_DIR


class JsonSnippet:

    def __init__(self):
        self.snippet_dir = RES_DIR
        self.snippets_file = os.path.join(self.snippet_dir, 'snippets.json')
        self.parse()

    def parse(self):
        if not os.path.isdir(self.snippet_dir):
            os.makedirs(RES_DIR, mode=755)
            self._create_snippet_file()
        elif not os.path.isfile(self.snippets_file):
            self._create_snippet_file()
        with open(self.snippets_file, mode='r', encoding='utf8') as sf:
            snippets_list = json.loads(sf.read())
        self.snippets_list = snippets_list

    def _create_snippet_file(self):
        self.snippets_list = [{'name': 'new', 'icon': 'appointment.svg',
                            'text': 'Put your text here'}, ]
        self.save_snippet_file()

    def get_snippet(self, index):
        return self.snippets_list[index]

    def save_snippet_file(self):
        with open(self.snippets_file, mode='w', encoding='utf8') as sf:
            sf.write(json.dumps(self.snippets_list))

    def add_snippet(self):
        empty_snippet = {'name': '', 'icon': 'appointment.svg',
                         'text': ''}
        self.snippets_list.append(empty_snippet)

    def save_snippet(self, index, name, icon, text):
        snippet = self.snippets_list[index]
        snippet['name'] = name
        snippet['icon'] = icon
        snippet['text'] = text

    def save_snippet_text(self, index, text):
        snippet = self.snippets_list[index]
        snippet['text'] = text

    def remove_snippet(self, index):
        del self.snippets_list[index]
